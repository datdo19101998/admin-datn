import React from 'react';

import {BrowserRouter , Switch } from 'react-router-dom'

import { ADMIN_ROUTES ,ROUTES } from './constant'

import AdminLayoutRoutes from './containers/AdminLayoutRoutes'

import DefaultLayoutRoutes from './containers/DefaultLayoutRoutes'


const App = () => {

  const renderAdminRoute =() =>{
    var xhtml = null;
    xhtml = ADMIN_ROUTES.map((route ,index)=>{
      return <AdminLayoutRoutes
        key={index}
        path={route.path}
        name={route.name}
        exact={route.exact}
        component={route.component}
      />
    })
    return xhtml;
  }
  const renderDefaultRoute =() =>{
    var xhtml = null;
    xhtml = ROUTES.map((route ,index)=>{
      return <DefaultLayoutRoutes
        key={index}
        path={route.path}
        name={route.name}
        exact={route.exact}
        component={route.component}
      />
    })
    return xhtml;
  }
  return (
    <BrowserRouter>
      <Switch>
        {renderAdminRoute()}
        {renderDefaultRoute()}
      </Switch>
    </BrowserRouter>
  );
}

export default App;
