import React from 'react';
import {Route} from 'react-router-dom'

const index = (props) => {
    const { component: YourComponent,name,...remainProps } = props;

    return (
      <Route
        {...remainProps}
        render={(routeProps) => {
          return <YourComponent name {...routeProps} />;
        }}
      />
    );
}
export default index;
