import React, {
  useEffect,
  useState
} from 'react';
import './styles.css';

import 'antd/dist/antd.css';

import {
  Link ,
  useHistory,
} from 'react-router-dom';

import * as SongAction from '../../redux/song/actions';
import * as SingerAction from '../../redux/singer/actions';
import * as AlbumAction from '../../redux/album/actions';
import * as AuthAction from '../../redux/auth/actions';

import {useDispatch} from 'react-redux'

import { Layout, Menu,Avatar , Popconfirm} from 'antd';

import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  VideoCameraOutlined,
  LogoutOutlined,
  AppstoreOutlined,
  BarChartOutlined,
  CloudOutlined,
  ShopOutlined,
  TeamOutlined,
  UserOutlined,
  UploadOutlined,
} from '@ant-design/icons';

import Logo from '../../assets/Dropbox-logo200.png'

import firebase from '../../firebase'

const { Header, Sider, Content  ,Footer } = Layout;


function App(props) {

  const history = useHistory();
  const dispatch = useDispatch();

  const [collapsed,setCollapsed] = useState(true);
  const [user, setUser] = useState(null)

  const {children} = props;

  // const pathname = children.props.location.pathname;

  useEffect(()=>{
    dispatch(SingerAction.getSinger());
    dispatch(SongAction.getSong());
    dispatch(AlbumAction.getAlbums()); 
    dispatch(AuthAction.getListen())
  },[dispatch])

  const toggle = () => {
    setCollapsed(!collapsed)
  };

  useEffect(()=>{
    firebase.auth().onAuthStateChanged((user)=>{
      if(!user){
        history.push('/login')
      }else {
        setUser(user)
      }
    })
  })

  function confirm(e) {
    firebase.auth().signOut().then(()=>{
      history.push('/login');
    }).catch(e => console.log(e.message))
  }

  function cancel(e) {
  }

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider       
      style={{
        overflow: 'auto',
        height: '100vh',
        position: 'fixed',
        left: 0
      }} height={"100vh"}  
      trigger={null} 
      collapsible 
      collapsed={collapsed}
      >
        {!collapsed
          ?
            <div>
              <div style={{justifyContent:'center',display:'flex',padding:10}}>
                <Avatar size={100} src="https://image.plo.vn/w800/uploaded/2020/aeixrdbkxq/2020_03_13/chi-pu2_txac.jpg" />
              </div>
              <div style={{justifyContent:'center',display:'flex',flexDirection:'column',alignItems:'center'}}>
                <h3 style={{color:'white', fontSize:20, fontWeight:'bold'}}>Đỗ Tiến Đạt</h3>
                <p style={{color:'white', fontSize:15}}>{user ?`Email : ${user.email }`: ''}</p>
              </div>
            </div>
          :
          <div style={{display:'flex',justifyContent:'center',marginTop:10,flexDirection:'column',alignItems:'center'}}>
            <Link to={'/'}>
              <img src={Logo} alt="" style={{height:35,width: 35}} />
              <div style={{color:'white'}}>Admin</div>
            </Link>
          </div>
          }
          <Menu
          theme={'dark'}
          mode={'inline'}
          >
            <Menu.Item icon={<VideoCameraOutlined />}>
              <Link to={'/song'}>Danh sách bài hát</Link>
            </Menu.Item>
            <Menu.Item icon={<VideoCameraOutlined />}>
              <Link to={'/singer'}>Danh sách Ca sĩ</Link>
            </Menu.Item>
            <Menu.Item icon={<VideoCameraOutlined />}>
              <Link to={'/album'}>Danh sách Albums</Link>
            </Menu.Item>
          </Menu>
      </Sider>
      <Layout className="site-layout">
        <Header className="site-layout-background" style={{ padding: "0 20px 0 0" ,display:'flex', flexDirection:'row',alignItems:'center',justifyContent:'space-between', height:69 }}>
          {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: 'trigger',
            onClick: toggle,
          })}
          <Popconfirm
            title="Are you want to sigout ?"
            onConfirm={confirm}
            onCancel={cancel}
            okText="Yes"
            cancelText="No"
          >
            <div style={{display: 'flex',flexDirection:'row',alignItems:'center',cursor:'pointer'}}>
              <LogoutOutlined style={{fontSize:28}} />
              <div style={{marginLeft:5}}>Logout</div>
            </div>
          </Popconfirm>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            margin: '24px 16px',
            padding: 10,
            minHeight: 280,
          }}
        >
          {children}
        </Content>
      </Layout>
    </Layout>
  //   <Layout>
  //   <Sider
      // style={{
      //   overflow: 'auto',
      //   height: '100vh',
      //   position: 'fixed',
      // }}
  //   >
  //     <div className="logo" />
  //     <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
  //       <Menu.Item key="1" icon={<UserOutlined />}>
  //         nav 1
  //       </Menu.Item>
  //       <Menu.Item key="2" icon={<VideoCameraOutlined />}>
  //         nav 2
  //       </Menu.Item>
  //       <Menu.Item key="3" icon={<UploadOutlined />}>
  //         nav 3
  //       </Menu.Item>
  //       <Menu.Item key="4" icon={<BarChartOutlined />}>
  //         nav 4
  //       </Menu.Item>
  //       <Menu.Item key="5" icon={<CloudOutlined />}>
  //         nav 5
  //       </Menu.Item>
  //       <Menu.Item key="6" icon={<AppstoreOutlined />}>
  //         nav 6
  //       </Menu.Item>
  //       <Menu.Item key="7" icon={<TeamOutlined />}>
  //         nav 7
  //       </Menu.Item>
  //       <Menu.Item key="8" icon={<ShopOutlined />}>
  //         nav 8
  //       </Menu.Item>
  //     </Menu>
  //   </Sider>
  //   <Layout className="site-layout" style={{ marginLeft: 200 }}>
  //       <Header className="site-layout-background" style={{ padding: "0 20px 0 0" ,display:'flex', flexDirection:'row',alignItems:'center',justifyContent:'space-between', height:69 }}>
  //          {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
  //            className: 'trigger',
  //            onClick: toggle,
  //          })}
  //          <Popconfirm
  //            title="Are you want to sigout ?"
  //            onConfirm={confirm}
  //            onCancel={cancel}
  //            okText="Yes"
  //            cancelText="No"
  //          >
  //            <div style={{display: 'flex',flexDirection:'row',alignItems:'center',cursor:'pointer'}}>
  //              <LogoutOutlined style={{fontSize:28}} />
  //              <div style={{marginLeft:5}}>Logout</div>
  //            </div>
  //          </Popconfirm>
  //        </Header>
  //         <Content
  //           className="site-layout-background"
  //           style={{
  //             margin: '24px 16px',
  //             padding: 10,
  //             minHeight: 280,
  //           }}
  //         >
  //           {children}
  //         </Content>
  //     <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
  //   </Layout>
  // </Layout>
  );
}

  export default App;
