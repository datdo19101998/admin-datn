import React from 'react';
import { Route } from 'react-router-dom';
import Dashboard from '../Dashboard'

const index = (props) => {
    const { component: YourComponent,name,...remainProps } = props;
    return (
        <Route 
           {...remainProps}
           render={routerProps => {
                return (
                    <Dashboard>
                        <YourComponent name={name} {...routerProps}/>
                    </Dashboard>
                )
           }}
        />
    );
}

export default index;
