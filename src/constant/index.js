import Home from '../components/Home';

import Forget from '../components/Forget'
import Login from './../components/Login';

import Songs from './../components/Songs'
import EditSong from './../components/Songs/editSong'
import AddSong from './../components/Songs/addSong'

import NotFound from './../components/404'

import Albums from '../components/Albums'
import EditAlbum from './../components/Albums/editAlbum'
import AddAlbum from '../components/Albums/addAlbum'
import AlbumDetail from './../components/Albums/detailAlbum'

import Singer from './../components/Singers'
import SingerDetail from './../components/Singers/singerDetail'
import EditSinger from './../components/Singers/editSinger'
import AddSinger from './../components/Singers/addSinger'


export const ADMIN_ROUTES = [
    {
        path: '/',
        name: 'Trang chủ',
        exact: true,
        component:  Home,
    },
    {
        path: '/song',
        name: 'danh sách bài hát',
        exact: true,
        component:  Songs,
    },
    {
        path: '/song-edit/:id',
        name: 'edit song',
        exact: true,
        component:  EditSong,
    },
    {
        path: '/song-add',
        name: 'add song',
        exact: true,
        component:  AddSong,
    },
    {
        path: '/singer',
        name: 'Danh sách ca sĩ',
        exact: true,
        component:  Singer,
    },
    {
        path: '/singer/:category',
        name: 'chi tiết ca sĩ',
        exact: true,
        component:  SingerDetail,
    },
    {
        path: '/singer-add',
        name: 'Them ca sĩ',
        exact: true,
        component:  AddSinger,
    },
    {
        path: '/singer-edit/:name',
        name: 'edit singer',
        exact: true,
        component:  EditSinger,
    },
    {
        path: '/album',
        name: 'Albums',
        exact: true,
        component:  Albums,
    },
    {
        path: '/album-edit/:id',
        name: 'edit album',
        exact: true,
        component:  EditAlbum,
    },
    {
        path: '/album/:category',
        name: 'Detail album',
        exact: true,
        component:  AlbumDetail,
    },
    {
        path: '/album-add',
        name: 'Detail album',
        exact: true,
        component:  AddAlbum,
    }
]

export const ROUTES = [
    {
        path: '/login',
        name: 'Đăng nhập',
        exact: true,
        component:  Login,
    },
    {
        path: '/forget',
        name: 'Foget password',
        exact: true,
        component:  Forget,
    },
    {
        path: '',
        exact: false,
        component:  NotFound,
    }
]
