import React from 'react';
import logo from '../../assets/404_Error.jpg'

class SiderDemo extends React.Component {
    render() {
      return (
        <div style={{flex: 1}}>
            <img src={logo} alt="" style={{ width:"100%", height:"100%" }} />
        </div>
      );
    }
  }

export default SiderDemo;
