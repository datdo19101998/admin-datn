import React, {
  useEffect, useState,
  // useState
} from 'react';
import {Layout, Row, Col, Card, Input,Button,Popconfirm,Table} from 'antd';
import './styles.css'
import firebase from '../../firebase';
import {
  useHistory,
} from 'react-router-dom';

import {isEmpty} from 'lodash';

import {useSelector,useDispatch} from 'react-redux'

import * as AuthAction from '../../redux/auth/actions';

import moment from "moment";


const {  Content } = Layout;


const renderImage = url => {
  return (
    <img 
    src={url} alt='' style={{height:50,width:50}} />
  )
}

const columns = [
  {
    title: 'Image',
    dataIndex: 'artwork',
    align: 'center',
    render: (artwork) => renderImage(artwork),
  },
  {
    title: 'Tên bài hát',
    dataIndex: 'title',
    align: 'center',
  },
  {
    title: 'Ca sĩ',
    dataIndex: 'artist',
    align: 'center',
  },
  {
    title: 'Số lượt nghe',
    dataIndex: 'price',
    align: 'center',
  },
];

const Home = () => {
  const history = useHistory();

  const dispatch = useDispatch();

  const [user, setUser] = useState(null);
  const [title,setTitle] = useState('Tất cả')
  const [checked,setChecked] = useState(true);

  const todayDate = moment().format("YYYY/MM/DD");
  const yesterdayDate = moment().subtract(1, "days").format("YYYY/MM/DD");

  const {statistical} = useSelector(state => state.Auth)


  useEffect(()=>{
    firebase.auth().onAuthStateChanged((user)=>{
      if(!user){
        history.push('/login')
      }else {
        setUser(user)
      }
    })
  })

  const getConditionalDate =(date) =>{
    if(!isEmpty(date)){
      dispatch(AuthAction.getListen(date))
      setTitle(date)
      setChecked(false)
    }else{
      dispatch(AuthAction.getListen())
      setChecked(true)
      setTitle('Tất cả')
    }
  }

  return (
    <Layout className="site-layout-background">
      <Content>
      <Row justify="center" gutter={[16,16]}>
        <Col sm={18}>
          <Row>
            <Col sm={16}>
              <Input placeholder="Basic usage" />
            </Col>
            <Col sm={8} style={{display:'flex',justifyContent:'space-around'}}>
              <Button disabled={checked} dis onClick={() =>getConditionalDate()} type="primary">Tất cả</Button>
              <Button onClick={() =>getConditionalDate(yesterdayDate)} type="primary">Hôm qua</Button>
              <Button onClick={() =>getConditionalDate(todayDate)} type="primary">hôm nay</Button>
            </Col>
          </Row>
        </Col>
      </Row>
        <Row justify="center" gutter={[16,16]}>
          <Col sm={18}>
          <Table               
          title={() => (
                <div style={{ fontWeight: "bold",textAlign: "center"}}>
                  {title}
                </div>
              )} 
          rowKey={'id'} 
          bordered 
          columns={columns} 
          dataSource={statistical} 
          size="middle" 
          />
          </Col>
        </Row>
      </Content>
    </Layout>
  );
}

export default Home;
