import React,{
    useEffect,
    useState
} from 'react';

import { Layout, Row , Col, Card, Button , Input,Pagination, Popconfirm } from 'antd';

import loadingIcon from '../../assets/loading.gif'

import {isEmpty} from 'lodash'

import {useSelector, useDispatch} from 'react-redux';

import * as SongAction from '../../redux/song/actions'

import {useHistory,useLocation} from 'react-router-dom'

import queryString from 'query-string'

const {  Content } = Layout;
const { Meta } = Card;

const Songs = () => {

    const history = useHistory();

    const dispatch = useDispatch();

    const {search} = useLocation();

    const parsed = queryString.parse(search);

    const page = parseInt(parsed.q) || 1;
    const perPage = 8;
    const start = (page - 1) * perPage;
    const end = page * perPage;

    const [items,setItems] = useState([])

    const {songs} = useSelector((state) => state.Songs);    

    useEffect(()=>{
        setItems(songs.slice(start,end));
    },[songs,start,end]);

    const onEditItem =(item) =>{
        const {id} = item;
        history.push(`/song-edit/${id}`)
    } 

    const confirm = (data) => {
        dispatch(SongAction.deleteSong(data))
    }
      
    const  cancel = (e) => {
        console.log(e);
    }

    const renderItem = () =>{
        var xhtml = null;
        if(!isEmpty(items)){
            xhtml = items.map((item,index) => {
                return <Col sm={6} key={index}>
                    <Card
                        hoverable
                        style={{ width: "100%"}}
                        cover={<img alt="example" src={item.artwork} style={{height:300}} />}
                    >
                        <Meta title={item.title} description={item.artist} />
                        <div style={{display:'flex', justifyContent:'space-between',marginLeft:-5}}>
                            <Button style={{marginTop:10}} type="primary" onClick={()=> onEditItem(item)}>Chỉnh sửa</Button>
                            <Popconfirm
                            title="Are you sure delete this task?"
                            onConfirm={()=>confirm(item)}
                            onCancel={cancel}
                            okText="Yes"
                            cancelText="No"
                        >
                            <Button
                            style={{marginTop:10}}
                            danger
                            >
                                Delete
                            </Button>
                        </Popconfirm>
                        </div>
                    </Card>
                </Col>
            })
        }
        return xhtml;
    }

    const onChange = (event) => {
        const {value} = event.target
        var data = songs.filter((item) => {
                return item.title.toLowerCase().includes(value.toLowerCase());
            })
        setItems(data)
    }

    function onChange1(pageNumber) {
        history.push(`/song?q=${pageNumber}`)
      }

    const onAddMusic = () => {
        history.push('/song-add')
    }

    return (
        <Layout className="site-layout-background">
            <Content>
                <Row justify={'center'} style={{padding:20}} gutter={[16, 24]}>
                    <Col sm={8}>
                        <Input
                            placeholder="Nhập để tìm kiếm bài hát..."
                            onChange={onChange}
                        />
                    </Col>
                    <Col>
                        <Button onClick={onAddMusic}>Thêm bài hát</Button>
                    </Col>
                </Row>
                <Row justify='center'>
                    <Col sm={20}>
                        {!isEmpty(songs)  ?
                        <div>
                            <Row gutter={[16, 24]}>
                                {renderItem()}
                            </Row>
                            <Row justify={'center'}>
                                <Col sm={15} style={{display:'flex',alignItems:'center',justifyContent:'center'}}>
                                    <Pagination showQuickJumper defaultCurrent={page} total={60} onChange={onChange1} />
                                </Col>
                            </Row>
                        </div>
                        :
                        <Row justify='center'>
                            <Col style={{display: 'flex', justify: 'center',alignItems:'center',paddingTop:'250px'}} sm={10}>
                                <img src={loadingIcon} alt="Loading..." className="icon" />
                            </Col>
                        </Row>}
                    </Col>
                </Row>
            </Content>
        </Layout>
    );
}

export default Songs;
