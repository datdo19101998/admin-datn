import React,{useEffect,useState} from 'react';
import {useParams} from "react-router-dom";
import {useDispatch,useSelector} from "react-redux"
import * as SongAction from '../../redux/song/actions'

import {Layout, Button, Space, Row, Col} from 'antd';

import { Form, Input,InputNumber } from 'formik-antd'
import { Formik } from 'formik'

import loadingIcon from '../../assets/loading.gif'

import firebaseApp from '../../firebase'


const {Content} = Layout;

const  EditSong =  () => {
    const {id} = useParams();
    const dispatch = useDispatch();
    const {song} = useSelector(state =>state.Songs)

    const [imageAsFile, setImageAsFile] = useState('')
    const [values,setValues] = useState(initialValues)
    const [loading,setLoading] = useState(true)

    useEffect(()=>{
        dispatch(SongAction.getSongItem(id))
    },[dispatch,id])

    useEffect(()=>{
        setValues(song)
        setTimeout(()=>{setLoading(false)},500)
    },[song])

    const handleFireBaseUpload = () => {
        console.log('start of upload')
        // async magic goes here...
        if(imageAsFile === '') {
            alert(`Vui lòng chọn ảnh trước khi áp dụng`)
        }
        const uploadTask = firebaseApp.storage().ref(`/pictures/${imageAsFile.name}`).put(imageAsFile)
        //initiates the firebase side uploading
        uploadTask.on('state_changed',
            (snapShot) => {
                //takes a snap shot of the process as it is happening
                console.log(snapShot)
            }, (err) => {
                //catches the errors
                console.log(err)
            }, () => {
                firebaseApp.storage().ref('pictures').child(imageAsFile.name).getDownloadURL()
                    .then(fireBaseUrl => {
                        setValues(prevObject => ({...prevObject, artwork: fireBaseUrl}))
                    })
            })
    }

    const handleImageAsFile =  (e) => {
        const image = e.target.files[0]
        setImageAsFile(imageFile => (image))
    }



    const onSubmit = (data) =>{
        dispatch(SongAction.updateSong(data))
    }

    return (
        <Layout className="site-layout-background">
            <Content>
                {!loading ? <Formik
                    onSubmit={onSubmit}
                    initialValues={values}
                    enableReinitialize={true}
                >
                    {({dirty, isSubmitting, resetForm}) => (
                        <Form>
                            <Row justify="end" style={{ marginBottom: 24 }}>
                                <Space align="end">
                                    <Button type="primary" disabled={!dirty}
                                        htmlType="submit"
                                        >Update</Button>
                                        <Button
                                        type="primary" 
                                        onClick={() => resetForm()}
                                        htmlType="button"
                                        disabled={!dirty || isSubmitting}
                                        >
                                            Reset
                                        </Button>
                                </Space>
                            </Row>
                            <Row justify="center" gutter={[16, 16]}>
                                <Col sm={8}>
                                    <img src={values.artwork}  style={{height:'450px', width:'100%'}} alt="" />
                                </Col>
                                <Col sm={14}>
                                    <Form.Item name={'title'} label="Name">
                                        <Input size='large' name={'title'} />
                                    </Form.Item>
                                    <Form.Item name={'artist'} label="Singer">
                                        <Input size='large' name={'artist'} />
                                    </Form.Item>
                                    <Form.Item name={'category1'} label="Category 1">
                                        <Input size='large'  name= {'category1'} />
                                    </Form.Item>
                                    <Form.Item name={'category2'} label="Category 2">
                                        <Input size='large' name= {'category2'} />
                                    </Form.Item>
                                    <Form.Item name={'artwork'} label="Ảnh mới">
                                        <div style={{display:'flex', flexDirection: 'row'}}>
                                            <input size='large'                     
                                            onChange={handleImageAsFile}
                                            type="file" 
                                            />
                                            <Button disabled={!imageAsFile} type="primary" onClick={handleFireBaseUpload}>Áp dụng</Button>
                                        </div>
                                    </Form.Item>
                                    <Form.Item name={'plays'} label="Plays">
                                        <InputNumber size='large'   name={'plays'} />
                                    </Form.Item>
                                    <Form.Item name={'likes'} label="Likes">
                                        <InputNumber size='large' name={'likes'} />
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Form>
                    )}
                </Formik>
            :
            <Row justify='center'>
                <Col style={{display: 'flex', justify: 'center',alignItems:'center',paddingTop:'250px'}} sm={10}>
                    <img src={loadingIcon} alt="Loading..." className="icon" />
                </Col>
            </Row>
            }
            </Content>
        </Layout>
    );
}
const initialValues = {
    title : '',
    artist:'',
    artwork: '',
    url: '',
    likes:0,
    plays: 0,
    category1: '',
    category2: ''
}

export default EditSong;
