import React,{
    useEffect,
    useState
} from 'react';
import firebaseApp from '../../firebase';

import { Layout} from 'antd';

const {  Content } = Layout;


const Songs = () => {

    // const [spells, setSpell] = useState([]);
    // const ref = firebaseApp.firestore().collection('songs');

    // const onCollectionUpdate = (querySnapshot) => {
    //   const boards = [];
    //   querySnapshot.forEach((doc) => {
    //   boards.push(doc.data());
    //   });
    //   setSpell(boards)
    // }

    // useEffect(()=>{
    //   ref.onSnapshot(onCollectionUpdate)
    // },[])

    const allInputs = {imgUrl: ''}
    const [imageAsFile, setImageAsFile] = useState('')
    const [imageAsUrl, setImageAsUrl] = useState(allInputs)

    const handleImageAsFile = (e) => {
        const image = e.target.files[0]
        setImageAsFile(imageFile => (image))
    }

    const handleFireBaseUpload = e => {
        e.preventDefault()
        console.log('start of upload')
        // async magic goes here...
        if(imageAsFile === '') {
            console.error(`not an image, the image file is a ${typeof(imageAsFile)}`)
        }
        const uploadTask = firebaseApp.storage().ref(`/images/${imageAsFile.name}`).put(imageAsFile)
        //initiates the firebase side uploading
        uploadTask.on('state_changed',
            (snapShot) => {
                //takes a snap shot of the process as it is happening
                console.log(snapShot)
            }, (err) => {
                //catches the errors
                console.log(err)
            }, () => {
                // gets the functions from storage refences the image storage in firebase by the children
                // gets the download url then sets the image from firebase as the value for the imgUrl key:
                firebaseApp.storage().ref('images').child(imageAsFile.name).getDownloadURL()
                    .then(fireBaseUrl => {
                        setImageAsUrl(prevObject => ({...prevObject, imgUrl: fireBaseUrl}))
                    })
            })
    }


    return (
        <div>
            <form onSubmit={handleFireBaseUpload}>
                <input
                    // allows you to reach into your file directory and upload image to the browser
                    type="file"
                    onChange={handleImageAsFile}
                />
                <button type={'submit'}>upload to firebase</button>
                <img src={imageAsUrl.imgUrl} alt="image tag" />
            </form>
        </div>
    );
}

export default Songs;
