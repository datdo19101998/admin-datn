import React,{useState} from 'react';
import {useDispatch} from "react-redux"
import * as SongAction from '../../redux/song/actions'

import validate from './validate'

import firebaseApp from '../../firebase'

import {Layout, Button, Space, Row, Col} from 'antd';

import { Form, Input,InputNumber } from 'formik-antd'
import { Formik } from 'formik'

const {  Content} = Layout;


const AddSong = () => {
    const dispatch = useDispatch();

    const [values,setValues] = useState(initialValues)

    const handleFireBaseUpload = (imageAsFile) => {
        console.log('start of upload')
        // async magic goes here...
        if(imageAsFile === '') {
            alert(`Vui lòng chọn ảnh trước khi áp dụng`)
        }
        const uploadTask = firebaseApp.storage().ref(`/pictures/${imageAsFile.name}`).put(imageAsFile)
        //initiates the firebase side uploading
        uploadTask.on('state_changed',
            (snapShot) => {
                //takes a snap shot of the process as it is happening
                console.log(snapShot)
            }, (err) => {
                //catches the errors
                console.log(err)
            }, () => {
                firebaseApp.storage().ref('pictures').child(imageAsFile.name).getDownloadURL()
                    .then(fireBaseUrl => {
                        setValues({...values,artwork:fireBaseUrl})
                    })
            })
    }

    const handleImageAsFile = e => {
        const image = e.target.files[0]
        if(image.type === 'image/jpeg'){
            handleFireBaseUpload(image);
        }else{
            alert('Vui lòng chọn lại định dạng file có đuôi .jpg')
        }
    }

    const handleFireBaseUploadMp3 = (imageAsFile) => {
        console.log('start of upload')
        // async magic goes here...
        if(imageAsFile === '') {
            alert(`Vui lòng chọn ảnh trước khi áp dụng`)
        }
        const uploadTask = firebaseApp.storage().ref(`/images/${imageAsFile.name}`).put(imageAsFile)
        //initiates the firebase side uploading
        uploadTask.on('state_changed',
            (snapShot) => {
                //takes a snap shot of the process as it is happening
                console.log(snapShot)
            }, (err) => {
                //catches the errors
                console.log(err)
            }, () => {
                firebaseApp.storage().ref('images').child(imageAsFile.name).getDownloadURL()
                    .then(fireBaseUrl => {
                        setValues({...values,url:fireBaseUrl})
                    })
            })
    }
    
    const handleMp3AsFile = e => {
        const file = e.target.files[0]
        handleFireBaseUploadMp3(file);
            // setImageAsFile(image)
    }

    const onSubmit = ({title,artist,artwork,url,likes,plays,category1,category2}) => {
        const newData = {
            name: title,
            singer: artist,
            url:{
            cover:artwork,
            play: url
            },
            category : [category1, category2],
            likes: likes,
            plays: plays
        }
        dispatch(SongAction.addSong(newData))
    }


    return (
        <Layout className="site-layout-background">
            <Content>
                <Formik
                    onSubmit={onSubmit}
                    initialValues={values}
                    enableReinitialize={true}
                    validationSchema={validate}
                >
                    {({dirty, isSubmitting, resetForm}) => (
                        <Form>
                            <Row justify="end" style={{ marginBottom: 24 }}>
                                <Space align="end">
                                    <Button type="primary" disabled={!dirty}
                                        htmlType="submit"
                                        >Add</Button>
                                        <Button
                                        type="primary" 
                                        onClick={() => resetForm()}
                                        htmlType="button"
                                        disabled={!dirty || isSubmitting}
                                        >
                                            Reset
                                        </Button>
                                </Space>
                            </Row>
                            <Row justify="center" gutter={[16, 16]}>
                                <Col sm={8}>
                                     <div style={{height:'450px', width:'100%', border:'1px solid gray',display:'flex',justify:'center',alignItems:'center'}}>
                                        {
                                        values.artwork ? 
                                        <img src={values.artwork}  style={{height:'100%', width:'100%'}} alt="" /> 
                                        :
                                        <input
                                        style={{marginLeft:100}}
                                         size='large'    
                                        placeholder={'click to add file'}                 
                                        onChange={handleImageAsFile}
                                        type="file" 
                                        />
                                        }
                                     </div>
                                </Col>
                                <Col sm={14}>
                                    <Form.Item name={'url'}>
                                        <div style={{display:'flex', justify: 'center',alignItems:'center',flexDirection: 'row'}}>
                                            <input
                                                size='large'    
                                                onChange={handleMp3AsFile}
                                                type="file" 
                                            />
                                            <Input disabled size='large' name={'url'} placeholder="Click to Choose file" />
                                        </div>
                                    </Form.Item>
                                    <Form.Item name={'title'} label="Name">
                                        <Input size='large' name={'title'} />
                                    </Form.Item>
                                    <Form.Item name={'artist'} label="Singer">
                                        <Input size='large' name={'artist'} />
                                    </Form.Item>
                                    <Form.Item name={'category1'} label="Category 1">
                                        <Input size='large' name= {'category1'} />
                                    </Form.Item>
                                    <Form.Item name={'category2'} label="Category 2">
                                        <Input size='large' name= {'category2'} />
                                    </Form.Item>
                                    <Form.Item name={'plays'} label="Plays">
                                        <InputNumber size='large'   name={'plays'} />
                                    </Form.Item>
                                    <Form.Item name={'likes'} label="Likes">
                                        <InputNumber size='large' name={'likes'} />
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Form>
                    )}
                </Formik>
            </Content>
        </Layout>
    );
}

const initialValues = {
    title : '',
    artist:'',
    artwork: '',
    url: '',
    likes:0,
    plays: 0,
    category1:'',
    category2:'',
}

export default AddSong;