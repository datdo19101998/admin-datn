import * as Yup from 'yup';

const validate = Yup.object().shape({
    title: Yup.string()
      .min(2, 'Too Short!')
      .max(50, 'Too Long!')
      .required('Title is required'),
    artist: Yup.string()
      .min(2, 'Too Short!')
      .max(50, 'Too Long!')
      .required('Artist is required'),
    category1: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('category1 is required'),
    category2: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('category2 is required'),
    artwork: Yup.string()
    .required('Artist is required'),
  });

export default validate;