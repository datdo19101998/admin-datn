import React,{useState} from 'react';

import {Button, Space, Row, Col} from 'antd';

import { Form, Input,InputNumber } from 'formik-antd'
import { Formik } from 'formik'

import * as SingerAction from '../../redux/singer/actions'

import {useDispatch} from 'react-redux'

import validate from './validate'


import firebaseApp from '../../firebase'

const AddSinger = () => {

    const dispatch = useDispatch();

    const [values,setValues] = useState(initialValues)

    const handleFireBaseUpload = (imageAsFile) => {
        console.log('start of upload')
        // async magic goes here...
        if(imageAsFile === '') {
            alert(`Vui lòng chọn ảnh trước khi áp dụng`)
        }
        const uploadTask = firebaseApp.storage().ref(`/pictures/${imageAsFile.name}`).put(imageAsFile)
        //initiates the firebase side uploading
        uploadTask.on('state_changed',
            (snapShot) => {
                //takes a snap shot of the process as it is happening
                console.log(snapShot)
            }, (err) => {
                //catches the errors
                console.log(err)
            }, () => {
                firebaseApp.storage().ref('pictures').child(imageAsFile.name).getDownloadURL()
                    .then(fireBaseUrl => {
                        setValues({...values,url:fireBaseUrl})
                    })
            })
    }

    const handleImageAsFile =  (e) => {
        const image = e.target.files[0]
        if(image.type === 'image/jpeg'){
            handleFireBaseUpload(image);
        }else{
            alert('Vui lòng chọn lại định dạng file có đuôi .jpg')
        }
    }


    const onSubmit = (data) =>{
        dispatch(SingerAction.addSinger(data))
    }

    return (
        <Formik
            onSubmit={onSubmit}
            initialValues={values}
            enableReinitialize={true}
            validationSchema={validate}
        >
            {({dirty, isSubmitting, resetForm}) => (
                <Form>
                    <Row justify="end" style={{ marginBottom: 24 }}>
                        <Space align="end">
                            <Button type="primary" disabled={!dirty}
                                htmlType="submit"
                                >Add</Button>
                                <Button
                                type="primary" 
                                onClick={() => resetForm()}
                                htmlType="button"
                                disabled={!dirty || isSubmitting}
                                >
                                    Reset
                                </Button>
                        </Space>
                    </Row>
                    <Row justify="center" gutter={[16, 16]}>
                        <Col sm={7}>
                            <div style={{height:'380px', width:'100%', border:'1px solid gray',display:'flex',justify:'center',alignItems:'center'}}>
                                {
                                values.url ? 
                                <img src={values.url}  style={{height:'100%', width:'100%'}} alt="" /> 
                                :
                                <input
                                style={{marginLeft:100}}
                                    size='large'    
                                placeholder={'click to add file'}                 
                                onChange={handleImageAsFile}
                                type="file" 
                                />
                                }
                            </div>
                        </Col>
                        <Col sm={14}>
                            <Form.Item name={'name'} label="Name">
                                <Input size='large' name={'name'} />
                            </Form.Item>
                            <Form.Item name={'description'} label="Mô tả">
                                <Input size='large' name={'description'} />
                            </Form.Item>
                            <Form.Item name={'category'} label="Category">
                                <Input size='large' name= {'category'} />
                            </Form.Item>
                            <Form.Item name={'url'} label="Image">
                                <Input size='large'  disabled name= {'url'} />
                            </Form.Item>
                            <Form.Item name={'hearts'} label="Hearts">
                                <InputNumber size='large'   name={'hearts'} />
                            </Form.Item>
                            <Form.Item name={'likes'} label="Likes">
                                <InputNumber size='large' name={'likes'} />
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            )}
        </Formik>
    );
}

const initialValues = {
    hearts : '',
    description:'',
    name: '',
    url: '',
    likes:'',
    category:''
}

export default AddSinger;
