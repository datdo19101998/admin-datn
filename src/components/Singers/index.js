import React,{
    useEffect,
    useState,
} from 'react';

import loadingIcon from '../../assets/giphy.gif'

import {useDispatch, useSelector} from 'react-redux'

import {Link, useHistory} from 'react-router-dom'

import './styles.css'
import 'antd/dist/antd.css';

import {isEmpty} from 'lodash'


import {Layout, Card, Row, Col,Modal,Button,Input,Popconfirm} from 'antd';



import * as SingerAction from '../../redux/singer/actions'

const {  Content } = Layout;

const { Meta } = Card;

const Singers = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const [items,setItems] = useState([])
    const [loading,setLoading] = useState(false)


    const {singers, informations} = useSelector(state => state.Singer);

    const onDescription = (item) => {
        setLoading(true);
        dispatch(SingerAction.getSingerDetail(item))
    }

    const onEditItem = item => {
        const {id} = item;
        history.push(`/singer-edit/${id}`)
    }

    useEffect(()=>{
        setItems(singers)
    },[dispatch, singers])

    const confirm = (data) => {
        dispatch(SingerAction.deleteSinger(data))
    }
      
    const  cancel = (e) => {
        console.log(e);
    }

    const renderItem = () =>{
        var xhtml = null;
        if(items){
            xhtml = items.map((item,index) => {
                return <Col sm={6} xs={24} key={index}>
                                <Card
                                    hoverable
                                    style={{ width: "100%"}}
                                    cover={<img alt="example" src={item.url} style={{height:300}} />}
                                >
                                    <Link to={`/singer/${item.category}`}>
                                        <Meta 
                                        style={{marginBottom:10}} 
                                        title={item.name} 
                                    />
                                    </Link>
                                    <div style={{display:'flex', justifyContent:'space-between',marginLeft:-5}}>
                                        <Button onClick={()=> onDescription(item)}>Mô tả</Button>
                                        <Button type="primary" onClick={()=> onEditItem(item)}>Chỉnh sửa</Button>
                                        <Popconfirm
                                            title="Are you sure delete this task?"
                                            onConfirm={()=>confirm(item)}
                                            onCancel={cancel}
                                            okText="Yes"
                                            cancelText="No"
                                        >
                                            <Button
                                            // style={{marginTop:10}}
                                            danger
                                            >
                                                Xóa
                                            </Button>
                                        </Popconfirm>
                                    </div>
                                </Card>
                        </Col>
            })
        }
        return xhtml;
    }

    const renderModal1 = () => {
        const {name, description, url} = informations
        return (
            <Modal
            title={`Ca sĩ - ${name}`}
            centered
            width="80%"
            visible={loading}
            bodyStyle={{height:300}}
            onOk={() => setLoading(false)}
            onCancel={onCancel}
                >
                    {!isEmpty(informations) ? 
                    <div style={{display:'flex', flexDirection: 'row'}}>
                        <div>
                            <img height="250" width='250' src={url} alt={`${name}`} />
                        </div>
                        <div style={{paddingLeft:10, width:'70%'}}>
                            <h3>Mô tả : {description}</h3>
                        </div>
                    </div> 
                    :
                    <div style={{alignItems:"center",display:'flex',paddingTop:50}} />
                    }
            </Modal>
        )
    }

    const onCancel =() => {
        setLoading(false)
        dispatch(SingerAction.resetSinger())
    }

    const onChange = (event) => {
        const {value} = event.target
        var data = singers.filter((item) => {
            return item.name.toLowerCase().includes(value.toLowerCase());
        })
        setItems(data)
    }

    const onAddSinger = () => {
        history.push('/singer-add')
    }

    return (
        <Layout className="site-layout-background">
            <Content>
                <Row justify={'center'} style={{padding:20}}>
                    <Col sm={8}>
                        <Input
                            placeholder="Search with song name ..."
                            onChange={onChange}
                        />
                    </Col>
                    <Col>
                        <Button onClick={onAddSinger}>Thêm Ca sĩ</Button>
                    </Col>
                </Row>
                {!isEmpty(singers) ?
                    <Row justify="center" gutter={[16, 24]}>
                        <Col sm = {20}>
                            <Row gutter={[16, 24]}>
                                {renderItem()}
                            </Row>
                        </Col>
                    </Row> :
                    <Row justify='center'>
                        <Col style={{display: 'flex', justify: 'center',alignItems:'center',paddingTop:'250px'}} sm={10}>
                            <img src={loadingIcon} alt="Loading..." className="icon" />
                        </Col>
                    </Row>
                }
                {renderModal1()}
            </Content>
        </Layout>
    );
}

export default Singers;
