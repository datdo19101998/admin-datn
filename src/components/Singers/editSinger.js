import React,{useEffect,useState} from 'react';

import {useDispatch, useSelector} from 'react-redux'
// import {isEmpty} from 'lodash';

import {useParams} from "react-router-dom";

import {Layout, Button, Space, Row, Col} from 'antd';

import { Form, Input,InputNumber } from 'formik-antd'
import { Formik } from 'formik'

import * as SingerAction from '../../redux/singer/actions'

import loadingIcon from '../../assets/loading.gif'

import firebaseApp from '../../firebase'

const {  Content } = Layout;

const EditSinger =() => {
    const dispatch = useDispatch();
    const {name} = useParams();  

    const {singers} = useSelector(state => state.Singer)

    const [values,setValues] = useState(initialValues)
    const [loading,setLoading] = useState(true)


    useEffect(()=>{
        const items = singers.filter(item => item.id === name)
        setValues(items[0])
        setTimeout(()=>{setLoading(false)},500)
    },[singers,name])

    const handleFireBaseUpload = (imageAsFile) => {
        console.log('start of upload')
        // async magic goes here...
        if(imageAsFile === '') {
            alert(`Vui lòng chọn ảnh trước khi áp dụng`)
        }
        const uploadTask = firebaseApp.storage().ref(`/pictures/${imageAsFile.name}`).put(imageAsFile)
        //initiates the firebase side uploading
        uploadTask.on('state_changed',
            (snapShot) => {
                //takes a snap shot of the process as it is happening
                console.log(snapShot)
            }, (err) => {
                //catches the errors
                console.log(err)
            }, () => {
                firebaseApp.storage().ref('pictures').child(imageAsFile.name).getDownloadURL()
                    .then(fireBaseUrl => {
                        setValues({...values,url:fireBaseUrl})
                    })
            })
    }

    const handleImageAsFile =  (e) => {
        const image = e.target.files[0]
        if(image.type === 'image/jpeg'){
            handleFireBaseUpload(image);
        }else{
            alert('Vui lòng chọn lại định dạng file có đuôi .jpg')
        }
    }


    const onSubmit = (data) =>{
        dispatch(SingerAction.updateSinger(data))
    }

    return (
        <Layout className="site-layout-background">
            <Content>
                {!loading ? <Formik
                    onSubmit={onSubmit}
                    initialValues={values}
                    enableReinitialize={true}
                >
                    {({dirty, isSubmitting, resetForm}) => (
                        <Form>
                            <Row justify="end" style={{ marginBottom: 24 }}>
                                <Space align="end">
                                    <Button type="primary" disabled={!dirty}
                                        htmlType="submit"
                                        >Update</Button>
                                        <Button
                                        type="primary" 
                                        onClick={() => resetForm()}
                                        htmlType="button"
                                        disabled={!dirty || isSubmitting}
                                        >
                                            Reset
                                        </Button>
                                </Space>
                            </Row>
                            <Row justify="center" gutter={[16, 16]}>
                                <Col sm={7}>
                                    {values ? <img src={values.url}  style={{height:'400px', width:'100%'}} alt="" /> : <div/>}
                                </Col>
                                <Col sm={14}>
                                    <Form.Item name={'name'} label="Name">
                                        <Input size='large' name={'name'} />
                                    </Form.Item>
                                    <Form.Item name={'description'} label="Description">
                                        <Input size='large' name={'description'} />
                                    </Form.Item>
                                    <Form.Item name={'category'} label="Category">
                                        <Input size='large' name= {'category'} />
                                    </Form.Item>
                                    <Form.Item name={'artwork'} label="Ảnh mới">
                                        <div style={{display:'flex', flexDirection: 'row'}}>
                                            <input size='large'                     
                                            onChange={handleImageAsFile}
                                            type="file" 
                                            />
                                            {/* <Button disabled={!imageAsFile} type="primary" onClick={handleFireBaseUpload}>Áp dụng</Button> */}
                                        </div>
                                    </Form.Item>
                                    <Form.Item name={'hearts'} label="Hearts">
                                        <InputNumber size='large'   name={'hearts'} />
                                    </Form.Item>
                                    <Form.Item name={'likes'} label="Likes">
                                        <InputNumber size='large' name={'likes'} />
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Form>
                    )}
                </Formik>
            :
            <Row justify='center'>
                <Col style={{display: 'flex', justify: 'center',alignItems:'center',paddingTop:'250px'}} sm={10}>
                    <img src={loadingIcon} alt="Loading..." className="icon" />
                </Col>
            </Row>
            }
            </Content>
        </Layout>
    );
}

const initialValues = {
    hearts : '',
    description:'',
    name: '',
    url: '',
    likes:0,
    category:''
}
export default EditSinger;