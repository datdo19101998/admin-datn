import React,{
    useEffect,
    useState
} from 'react';

import loadingIcon from '../../assets/giphy.gif'

import {Layout, Row, Col, Card, Input,Button,Popconfirm} from 'antd';

import {useSelector,useDispatch} from 'react-redux'

import {isEmpty} from 'lodash'

import {useHistory,Link} from 'react-router-dom'

import * as AlbumAction from '../../redux/album/actions'

const {  Content } = Layout;
const { Meta } = Card;



const Songs = () => {

    const dispatch = useDispatch();

    const history = useHistory();

    const [items, setItems] = useState([]);

    const {albums} = useSelector(state => state.Album)

    useEffect(()=>{
        dispatch(AlbumAction.getAlbums())
    },[dispatch])

    useEffect(()=>{
        setItems(albums)
    },[albums])

    const confirm = (item) =>{
        dispatch(AlbumAction.deleteAlbum(item))
    }

    const cancel = () =>{

    }

    const onEditItem = ({id}) =>{
        history.push(`/album-edit/${id}`)
    }

    const onAddAlbum = () =>{
        history.push(`/album-add`)
    }

    const renderItem = () =>{
        var xhtml = null;
        if(items){
            xhtml = items.map((item,index) => {
                return <Col sm={6} key={index}>
                    <Card
                        hoverable
                        style={{ width: "100%"}}
                        cover={<img alt="example" src={item.url} style={{height:300}} />}
                    >
                        <Meta description={item.singer} style={{marginBottom:10}} />
                            <Link to={`/album/${item.category}`}>
                                <Meta 
                                style={{marginBottom:10}} 
                                title={item.name} 
                                />
                            </Link>
                            <div style={{display:'flex', justifyContent:'space-between',marginLeft:-5}}>
                                {/* <Button 
                                // onClick={()=> onDescription(item)}
                                >
                                    Mô tả
                                </Button> */}
                                <Button
                                 type="primary" 
                                 style={{marginBottom:10}}
                                 onClick={()=> onEditItem(item)}
                                 >
                                     Chỉnh sửa
                                </Button>
                                <Popconfirm
                                    title="Are you sure delete this task?"
                                    onConfirm={()=>confirm(item)}
                                    onCancel={cancel}
                                    okText="Yes"
                                    cancelText="No"
                                >
                                    <Button
                                    style={{marginBottom:10}}
                                    // style={{marginTop:10}}
                                    danger
                                    >
                                        Xóa album
                                    </Button>
                                </Popconfirm>
                            </div>
                    </Card>
                </Col>
            })
        }
        return xhtml;
    }

    const onChange = (event) => {
        const {value} = event.target
        var data = albums.filter((item) => {
            return item.name.toLowerCase().includes(value.toLowerCase());
        })
        setItems(data)
    }

    return (
        <Layout className="site-layout-background">
            <Content>
                <Row justify={'center'} style={{padding:20}}>
                    <Col sm={8}>
                        <Input
                            placeholder="Search with song name ..."
                            onChange={onChange}
                        />
                    </Col>
                    <Col>
                        <Button onClick={onAddAlbum}>Thêm Album</Button>
                    </Col>
                </Row>
                <Row justify={'center'}>
                <Col sm={20}>
                        {!isEmpty(albums) ?
                        <Row gutter={[16, 24]}>
                            {renderItem()}
                        </Row> :
                        <Row justify='center'>
                            <Col style={{display: 'flex', justify: 'center',alignItems:'center',paddingTop:'250px'}} sm={10}>
                                <img src={loadingIcon} alt="Loading..." className="icon" />
                            </Col>
                        </Row>}
                    </Col>
                </Row>
            </Content>
        </Layout>
    );
}

export default Songs;
