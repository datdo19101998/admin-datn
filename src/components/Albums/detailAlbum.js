import React,{useEffect,useState} from 'react';

// import './styles.css'

import {useDispatch, useSelector} from 'react-redux'
import { Layout, Row , Col, Card, Popover, Input} from 'antd';
import {isEmpty} from 'lodash';

import {useParams} from "react-router-dom";
import * as albumAction from '../../redux/album/actions'

import loadingIcon from '../../assets/loading.gif'

import {
    MoreOutlined,
} from '@ant-design/icons';

const { Meta } = Card;
const {  Content } = Layout;




const DetailAlbum = () => {

    const dispatch = useDispatch();
    
    const [items,setItems] = useState([])
    const [loading,setLoading] = useState(true)

    const {category} = useParams();    

    const {album} = useSelector(state => state.Album)
    
    useEffect(()=>{
        dispatch(albumAction.getAlbumDetails({category}))
    }, [dispatch, category])

    useEffect(()=>{
        setItems(album)
        setTimeout(()=>{setLoading(false)},600)
    },[album])

    console.log(items)
    
    const content = (
        <div>
            <p>Chi tiết</p>
            <p>Chỉnh sửa</p>
        </div>
    );

    const renderItem = () =>{
        var xhtml = null;
        if(!isEmpty(items)){
            xhtml = items.map((item,index) => {
                return <Col sm={6} key={index}>
                    <Card
                        hoverable
                        style={{ width: "100%"}}
                        cover={<img alt="example" src={item.artwork} style={{height:300}} />}
                    >
                        <Meta title={item.title} description={item.artist} />
                        <div style={{display:'flex', justifyContent:'flex-end', alignItems:'flex-end'}}>
                            <Popover content={content} title="Hành động">
                                <MoreOutlined style={{fontSize:25}} />
                            </Popover>
                        </div>
                    </Card>
                </Col>
            })
        }
        return xhtml;
    }

    const onChange = (event) => {
        const {value} = event.target
        var data = album.filter((item) => {
                return item.title.toLowerCase().includes(value.toLowerCase());
            })
        setItems(data)
    }

    return (
        <Layout className="site-layout-background">
            <Content>
                <Row justify={'center'} style={{padding:20}}>
                    <Col sm={8}>
                        <Input
                            placeholder="Search with song name ..."
                            onChange={onChange}
                        />
                    </Col>
                </Row>
                <Row justify={'center'}>
                    <Col sm={20}>
                        {!loading ?
                        <Row gutter={[16, 24]}>
                            {renderItem()}
                        </Row> :
                        <Row justify='center'>
                            <Col style={{display: 'flex', justify: 'center',alignItems:'center',paddingTop:'250px'}} sm={10}>
                                <img src={loadingIcon} alt="Loading..." className="icon" />
                            </Col>
                        </Row>}
                    </Col>
                </Row>
            </Content>
        </Layout>
    )
}

export default DetailAlbum;
