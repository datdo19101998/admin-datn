import * as Yup from 'yup';

const validate = Yup.object().shape({
    name: Yup.string()
      .required('name is required'),
    description: Yup.string()
      .required('description is required'),
    category: Yup.string()
    .required('category is required'),
    url: Yup.string()
    .required('url is required'),
    likes: Yup.string()
    .required('likes is required'),
  });

export default validate;