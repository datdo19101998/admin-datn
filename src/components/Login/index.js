import React,{useEffect,useState} from 'react';
import 'antd/dist/antd.css';
import './styles.css';
import { Form, Input, Button, Row, Col, Modal } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import {Link, useHistory} from 'react-router-dom';
import firebase from "../../firebase";

const Login = () => {
    const history = useHistory();

    const [user,setUser] = useState(null);
    const [isReady,setIsReady] = useState(false)


    useEffect(()=>{
        firebase.auth().onAuthStateChanged((user)=>{
            if(user){
                setUser(user)
                setIsReady(true)
            }
        })
    },[user])

    useEffect(()=>{
        if (isReady){
            history.push('/')
        }
    },[history, isReady])

    const [visible,setVisible] = useState(false)

   const  handleOk = e => {
       setVisible(false)
   };

    const handleCancel = e => {
        setVisible(false);
        firebase.auth().signOut();
    };

  const onFinish = values => {
    const { email, password } = values
    if(email === 'admin@gmail.com'){
      firebase.auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
          setIsReady(true)
      })
      .catch(function (error) {
          alert(error);
      });
    }else{
      alert('Vui lòng đăng nhập bằng tài khoản của quản trị viên')
    }
  };

  return (
      <div className={'imageBackground'}>
        <Row justify={'center'}>
            <Col sm={12}>
                <div className={'imageContent'}>
                    <div className="content-agile1">
                        <h2 className="agileits1">Music Official</h2>
                        <p className="agileits2">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                </div>
            </Col>
            <Col sm={12}>
                <div style={{height:'100%', width:'100%',backgroundColor:'#201E1E',paddingTop:150}}>
                    <Row justify={'center'}>
                         <Col jusity={'center'} sm={15}>
                         <div style={{textAlign:'center'}}>
                            <h1 style={{color:'white'}}>Đăng nhập</h1>
                        </div>
                          <Form
                            name="normal_login"
                            className="login-form"
                            initialValues={{
                              remember: true,
                            }}
                            onFinish={onFinish}
                          >
                            <Form.Item
                              name="email"
                              rules={[
                                {
                                  required: true,
                                  message: 'Please input your email !',
                                },
                              ]}
                            >
                              <Input size={'large'} prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email" />
                            </Form.Item>
                            <Form.Item
                              name="password"
                              rules={[
                                {
                                  required: true,
                                  message: 'Please input your Password!',
                                },
                              ]}
                            >
                              <Input
                                  size={'large'}
                                prefix={<LockOutlined className="site-form-item-icon" />}
                                type="password"
                                placeholder="Password"
                              />
                            </Form.Item>

                            <Form.Item>
                              <Button size={'large'} type="primary" htmlType="submit" className="login-form-button">
                                  Đăng nhập
                              </Button>
                              <Link to={'forget'}>Forget password</Link>
                            </Form.Item>
                          </Form>
                        </Col>
                      </Row>
                </div>
            </Col>
        </Row>
        <Modal
            title="Thông báo !"
            visible={visible}
            onOk={handleOk}
            onCancel={handleCancel}
            centered
        >
            <p>Đăng nhập thành công...</p>
        </Modal>
      </div>
  );
};
export default Login
