import * as types from '../../constants/singer';

const initialState = {
  singers: [],
  singer: [],
  informations: {},
  error: null,
  message: ''
};

const reducer = (
  state = initialState,
  {type, singers, singer, error, informations,message,id},
) => {
  switch (type) {
    case types.GET_SINGER_SUCCESS:
      return {...state, singers: singers};
    case types.GET_SINGER_ERROR:
      return {...state, error: error};
    case types.GET_SINGER_DETAIL_SUCCESS:
      return {...state, singer: singer, informations: informations};
    case types.GET_SINGER_DETAIL_ERROR:
      return {...state, error: error};
    case types.RESET_SINGER_SUCCESS:
      return {...state, informations:{}}
    case types.UPDATE_SINGER_SUCCESS:
      return {...state, message:message}
    case types.ADD_SINGER_SUCCESS:
      return {...state, message:message}
    case types.DELETE_SINGER_SUCCESS:
      return {...state,singers: state.singers.filter((item) => item.id !== id)}
    default:
      return {...state};
  }
};

export default reducer;
