import * as types from '../../constants/singer';

import {all, takeEvery, put, fork, call} from 'redux-saga/effects';

import * as Service from '../../service/singer';

export function* getSinger() {
  yield takeEvery(types.GET_SINGER_REQUEST, function* () {
    try {
      const resp = yield call(Service.getSingers);

      yield put({
        type: types.GET_SINGER_SUCCESS,
        singers: resp,
      });
    } catch (err) {
      yield put({
        type: types.GET_SINGER_ERROR,
        error: err.message,
      });
    }
  });
}

export function* getSingerDetail() {
  yield takeEvery(types.GET_SINGER_DETAIL_REQUEST, function* ({payload}) {
    // const {category} = payload;
    try {
      const resp = yield call(Service.getSingerDetail, payload);
      yield put({
        type: types.GET_SINGER_DETAIL_SUCCESS,
        singer: resp,
        informations: payload,
      });
    } catch (err) {
      yield put({type: types.GET_SINGER_DETAIL_ERROR, error: err.message});
    }
  });
}

export function* updateSinger() {
  yield takeEvery(types.UPDATE_SINGER_REQUEST, function* ({payload}) {
    try {
      const message = yield call(Service.updateSinger(payload));
      yield put({
        type: types.UPDATE_SINGER_SUCCESS,
        message: message
      })
    }catch (err) {
      yield put({type: types.GET_SINGER_DETAIL_ERROR, error: err.message});
    }
  });
}

export function* addSinger() {
  yield takeEvery(types.ADD_SINGER_REQUEST, function* ({payload}) {
    // console.log(payload)
    try {
      const resp = yield call(Service.addSinger(payload));
      yield put({
        type: types.ADD_SINGER_SUCCESS,
        resp: resp
      })
    }catch (err) {
      yield put({type: types.GET_SINGER_DETAIL_ERROR, error: err.message});
    }
  });
}

export function* resetSinger() {
  yield takeEvery(types.RESET_SINGER_REQUEST, function* () {
    yield put({type: types.RESET_SINGER_SUCCESS})
  });
}

export function* deleteSinger() {
  yield takeEvery(types.DELETE_SINGER_REQUEST, function* ({payload}) {
    const {id} = payload;
    try{
      const resp = yield call(Service.deleteSinger, payload)
      yield put({
        type: types.DELETE_SINGER_SUCCESS, 
        message: resp,
        id: id
      })
    }catch (err) {
      yield put({type: types.GET_SINGER_DETAIL_ERROR, error: err.message});
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getSinger)]);
  yield all([fork(getSingerDetail)]);
  yield all([fork(resetSinger)]);
  yield all([fork(updateSinger)]);
  yield all([fork(addSinger)]);
  yield all([fork(deleteSinger)]);
}
