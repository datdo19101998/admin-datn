import * as types from '../../constants/singer';

export const getSinger = () => ({
  type: types.GET_SINGER_REQUEST,
});

export const getSingerDetail = (payload) => ({
  type: types.GET_SINGER_DETAIL_REQUEST,
  payload,
});

export const resetSinger = () => ({
  type: types.RESET_SINGER_REQUEST,
})

export const updateSinger = (payload) => ({
  type: types.UPDATE_SINGER_REQUEST,
  payload,
})

export const addSinger = (payload) => ({
  type: types.ADD_SINGER_REQUEST,
  payload,
})

export const deleteSinger = (payload) => ({
  type: types.DELETE_SINGER_REQUEST,
  payload,
})

