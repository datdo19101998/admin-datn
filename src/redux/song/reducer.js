import * as types from '../../constants/song';

const initialState = {
  songs: [],
  song: {},
  error: null,
  message: '',
  fileUpload: {}
};
const reducer = (state = initialState, {type, songs, song, error,message,file,id}) => {
  switch (type) {
    case types.GET_SONG_SUCCESS:
      return {
        ...state,
        songs: songs,
      };
    case types.GET_SONG_ERROR:
      return {
        ...state,
        error: error,
      };
    case types.GET_SONG_PLAYING_SUCCESS:
      return {...state, song: song};
    case types.UPDATE_SONG_SUCCESS:
      return {...state};
    case types.UPLOAD_FILE_SUCCESS:
      return {
        ...state,
        fileUpload: file
      }
    case types.ADD_SONG_SUCCESS:
      alert(message)
      return {...state}
    case types.DELETE_SONG_SUCCESS:
      alert(message)
      return {...state,songs: state.songs.filter((item) => item.id !== id)}
    default:
      return {...state};
  }
};
export default reducer;
