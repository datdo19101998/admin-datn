import * as songConstant from '../../constants/song';

export const getSong = () => ({
  type: songConstant.GET_SONG_REQUEST,
});

export const getSongItem = (payload) => ({
  type: songConstant.GET_SONG_PLAYING,
  payload,
});

export const updateSong = (payload) => ({
  type: songConstant.UPDATE_SONG_REQUEST,
  payload,
});

export const uploadFile = (payload) => ({
  type: songConstant.UPLOAD_FILE_REQUEST,
  payload,
})

export const addSong = (payload) => ({
  type: songConstant.ADD_SONG_REQUEST,
  payload,
});

export const deleteSong = (payload) => ({
  type: songConstant.DELETE_SONG_REQUEST,
  payload,
});

