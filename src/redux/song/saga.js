import * as types from '../../constants/song';
import {all, takeEvery, put, fork, call} from 'redux-saga/effects';

import * as Service from '../../service/song';

export function* getSong() {
  yield takeEvery(types.GET_SONG_REQUEST, function* () {
    try {
      const resp = yield call(Service.getSong);
      yield put({
        type: types.GET_SONG_SUCCESS,
        songs: resp,
      });
    } catch (err) {
      yield put({
        type: types.GET_SONG_ERROR,
        error: err.message,
      });
    }
  });
}

export function* getSongPlaying() {
  yield takeEvery(types.GET_SONG_PLAYING, function* ({payload}) {
    try{
      const resp = yield call(Service.getSongItem,payload)
      yield put({
        type: types.GET_SONG_PLAYING_SUCCESS,
        song: resp
      })
    }catch(err){
      yield put({
        type: types.GET_SONG_ERROR,
        error: err.message,
      });
    }
  });
}

export function* updateSong() {
  yield takeEvery(types.UPDATE_SONG_REQUEST, function* ({payload}) {
    try{
      yield call(Service.updateSong,payload)
      yield put({
        type: types.UPDATE_SONG_SUCCESS
      })
    }catch(err){
      console.log(err)
      yield put({
        type: types.GET_SONG_ERROR,
        error: err.message,
      });
    }
  });
}

export function* uploadFile (){
  yield takeEvery(types.UPLOAD_FILE_REQUEST,function* ({payload}) {
    const {image,resolve,reject} = payload;
    try {
      const response = yield call(Service.uploadFile,image)
      // console.log('response',response)
      yield put({
        type: types.UPLOAD_FILE_SUCCESS,
        file: response
      })
      yield resolve();
    }catch(err){
      yield put({
        type: types.GET_SONG_ERROR,
        error: err.message,
      });
      yield reject(err);
    }
  })
}

export function* addSong (){
  yield takeEvery(types.ADD_SONG_REQUEST,function* ({payload}) {
    try {
      const message = yield call(Service.addSong,payload);
      yield put({
        type: types.ADD_SONG_SUCCESS,
        message: message
      })
    }catch(err){
      yield put({
        type: types.GET_SONG_ERROR,
        error: err.message,
      });
    }
  })
}

export function* deleteSong (){
  yield takeEvery(types.DELETE_SONG_REQUEST,function* ({payload}) {
    // console.log(payload)
    const {id} = payload;
    try {
      const message = yield call(Service.deleteSong, payload);
      yield put({
        type: types.DELETE_SONG_SUCCESS,
        id :id,
        message: message
      })
    }catch(err){
      yield put({
        type: types.GET_SONG_ERROR,
        error: err.message,
      });
    }
  })
}

export default function* rootSaga() {
  yield all([fork(getSong)]);
  yield all([fork(getSongPlaying)]);
  yield all([fork(updateSong)]);
  yield all([fork(uploadFile)]);
  yield all([fork(addSong)]);
  yield all([fork(deleteSong)]);
}
