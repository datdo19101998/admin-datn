import * as types from '../../constants/auth';

const initialState = {
  user: {},
  error: null,
  recent: [],
  statistical:[],
  message: '',
};

const reducer = (
  state = initialState,
  {type, user, recent, error, message,statistical},
) => {
  switch (type) {
    case types.POST_USER_SUCCESS:
      return {
        ...state,
        message: message,
      };
    case types.POST_USER_ERROR:
      return {
        ...state,
        error: error,
      };
    case types.GET_CURRENT_USER_SUCCESS:
      return {
        ...state,
        user: user,
      };
    case types.GET_CURRENT_USER_ERROR:
      return {
        ...state,
        error: error,
      };
    case types.GET_RECENT_SUCCESS:
      return {
        ...state,
        recent: recent,
      };
    case types.GET_RECENT_ERROR:
      return {
        ...state,
        error: error,
      };
    case types.GET_LISTEN_SUCCESS:
      return {
        ...state,
        statistical: statistical,
      };
    default:
      return {...state};
  }
};

export default reducer;
