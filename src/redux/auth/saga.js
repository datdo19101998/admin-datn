import * as types from '../../constants/auth';

import * as Service from '../../service/auth';

import {takeEvery, all, call, put, fork} from 'redux-saga/effects';

export function* postUser() {
  yield takeEvery(types.POST_USER_REQUEST, function* () {
    try {
      const resp = yield call(Service.postUser);
      console.log('message', resp);
      yield put({
        type: types.POST_USER_SUCCESS,
        message: resp,
      });
    } catch (e) {
      yield put({
        type: types.POST_USER_ERROR,
        error: e.message,
      });
    }
  });
}

export function* getCurrentUser() {
  yield takeEvery(types.GET_CURRENT_USER_REQUEST, function* () {
    try {
      const resp = yield call(Service.getCurrentUser);

      yield put({
        type: types.GET_CURRENT_USER_SUCCESS,
        user: resp,
      });
    } catch (e) {
      yield put({
        type: types.GET_CURRENT_USER_ERROR,
        error: e.message,
      });
    }
  });
}

export function* getRecent() {
  yield takeEvery(types.GET_RECENT_REQUEST, function* ({payload}) {
    const {uid} = payload;
    try {
      const resp = yield call(Service.getRecent, uid);
      yield put({
        type: types.GET_RECENT_SUCCESS,
        recent: resp,
      });
    } catch (e) {
      yield put({
        type: types.GET_RECENT_ERROR,
        error: e.message,
      });
    }
  });
}

export function* getListen() {
  yield takeEvery(types.GET_LISTEN_REQUEST, function* ({payload}) {
    try {
      const resp = yield call(Service.getListen,payload);
      yield put({
        type: types.GET_LISTEN_SUCCESS,
        statistical: resp,
      });
    } catch (e) {
      yield put({
        type: types.GET_RECENT_ERROR,
        error: e.message,
      });
    }
  });
}

export default function* rootSaga() {
  yield all([fork(postUser)]);
  yield all([fork(getCurrentUser)]);
  yield all([fork(getRecent)]);
  yield all([fork(getListen)]);
}
