import * as types from '../../constants/auth';

export const loginSaga = (payload) => ({
  type: types.LOGIN_REQUEST,
  payload
})

export const postUser = () => ({
  type: types.POST_USER_REQUEST,
});

export const getCurrentUser = () => ({
  type: types.GET_CURRENT_USER_REQUEST,
});

export const getRecent = (payload) => ({
  type: types.GET_RECENT_REQUEST,
  payload,
});

export const getListen = (payload) => ({
  type: types.GET_LISTEN_REQUEST,
  payload
});
