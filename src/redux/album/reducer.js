import * as types from '../../constants/album';

const initialState = {
  albums: [],
  album: [],
  name: '',
  error: null,
  message: '',
};

const reducer = (state = initialState, {type, album, albums, error, name,message,id}) => {
  switch (type) {
    case types.GET_ALBUM_SUCCESS:
      return {...state, albums: albums};
    case types.GET_ALBUM_ERROR:
      return {...state, error: error};
    case types.GET_ALBUM_DETAIL_SUCCESS:
      return {...state, album: album, name: name};
    case types.GET_ALBUM_DETAIL_ERROR:
      return {...state, error: error};
    case types.EDIT_ALBUM_SUCCESS:
      return {...state, message: message}
    case types.ADD_ALBUM_REQUEST:
      return {...state, message: message}
    case types.DELETE_ALBUM_SUCCESS:
      return {...state,albums: state.albums.filter((item) => item.id !== id)}
    default:
      return {...state};
  }
};

export default reducer;
