import * as types from '../../constants/album';

export const getAlbums = () => ({
  type: types.GET_ALBUM_REQUEST,
});

export const getAlbumDetails = (payload) => ({
  type: types.GET_ALBUM_DETAIL_REQUEST,
  payload,
});

export const editAlbum = (payload) => ({
  type: types.EDIT_ALBUM_REQUEST,
  payload,
});

export const addAlbum = (payload) => ({
  type: types.ADD_ALBUM_REQUEST,
  payload,
});

export const deleteAlbum = (payload) => ({
  type: types.DELETE_ALBUM_REQUEST,
  payload,
})