import * as types from '../../constants/album';

import * as Service from '../../service/album';

import {takeEvery, all, call, put, fork} from 'redux-saga/effects';

export function* getAlbums() {
  yield takeEvery(types.GET_ALBUM_REQUEST, function* () {
    try {
      const resp = yield call(Service.getAlbums);
      yield put({
        type: types.GET_ALBUM_SUCCESS,
        albums: resp,
      });
    } catch (err) {
      yield put({
        type: types.GET_ALBUM_ERROR,
        error: err.message,
      });
    }
  });
}

export function* getAlbumDetail() {
  yield takeEvery(types.GET_ALBUM_DETAIL_REQUEST, function* ({payload}) {
    try {
      const resp = yield call(Service.getAlbumDetails, payload);

      yield put({
        type: types.GET_ALBUM_DETAIL_SUCCESS,
        album: resp,
      });
    } catch (err) {
      yield put({
        type: types.GET_ALBUM_DETAIL_ERROR,
        error: err.message,
      });
    }
  });
}

export function* editAlbum() {
  yield takeEvery(types.EDIT_ALBUM_REQUEST, function* ({payload}) {
    try {
      const resp = yield call(Service.editAlbum, payload)
      yield put({
        type: types.EDIT_ALBUM_SUCCESS,
        message: resp
      })
    }catch(err) {
      yield put({
        type: types.GET_ALBUM_DETAIL_ERROR,
        error: err.message,
      });
    }
  })
}

export function* addAlbum() {
  yield takeEvery(types.ADD_ALBUM_REQUEST, function* ({payload}) {
    // console.log(payload)
    try {
      const resp = yield call(Service.addAlbum, payload)
      yield put({
        type: types.ADD_ALBUM_SUCCESS,
        message: resp
      })
    }catch(err) {
      yield put({
        type: types.GET_ALBUM_DETAIL_ERROR,
        error: err.message,
      });
    }
  })
}

export function* deleteAlbum() {
  yield takeEvery(types.DELETE_ALBUM_REQUEST, function* ({payload}) {

    const {id} = payload;
    try {
      const resp = yield call(Service.deleteAlbum, payload)
      yield put({
        type: types.DELETE_ALBUM_SUCCESS,
        id: id,
        message: resp
      })
    }catch(err) {
      yield put({
        type: types.GET_ALBUM_DETAIL_ERROR,
        error: err.message,
      });
    }
  })
}

export default function* rootSaga() {
  yield all([fork(getAlbums)]);
  yield all([fork(getAlbumDetail)]);
  yield all([fork(editAlbum)]);
  yield all([fork(addAlbum)]); 
  yield all([fork(deleteAlbum)]);
}
