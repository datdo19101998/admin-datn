import firebase from "./../firebase";
import {sortBy} from 'lodash';

const ref3 = firebase.firestore().collection('songs');

export const getSong = async () => {
  let querySnapshot = await ref3.get();
  const songs = [];
  querySnapshot.forEach((doc) => {
    songs.push({
      id: doc.id,
      title: doc.data().name,
      artist: doc.data().singer,
      artwork: doc.data().url.cover,
      url: doc.data().url.play,
      likes: doc.data().likes,
      plays: doc.data().plays,
      category1: doc.data().category[0],
      category2: doc.data().category[1]
    });
  });
  const listSongs = sortBy(songs, 'title')
  return listSongs;
};

export const getSongItem = async (id) =>{
    let ref3 = firebase.firestore().collection('songs').doc(id);
    var data = {}
    await ref3.get().then(function(doc) {
        if (doc.exists) {
          // console.log("Document data:", doc.data());
          data = {
            id: doc.id,
            title: doc.data().name,
            artist: doc.data().singer,
            artwork: doc.data().url.cover,
            url: doc.data().url.play,
            likes: doc.data().likes,
            plays: doc.data().plays,
            category1: doc.data().category[0],
            category2: doc.data().category[1]          }
        } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
        }
    }).catch(function(error) {
        console.log("Error getting document:", error);
    });
    return data;
}

export const updateSong = (data) => {
  const {id,title,artist,artwork,category1,category2,likes,plays,url} = data;
  firebase.firestore().collection('songs').doc(id).update({
    name: title,
    singer: artist,
    url:{
      cover:artwork,
      play: url
    },
    category : [category1,category2],
    likes: likes,
    plays: plays
  }).then(() => {
    alert('Song is updated!');
  });
}

export const uploadFile = (imageAsFile) => {
  // console.log(imageAsFile);
  console.log('start of upload')
  if(imageAsFile === '') {
      alert(`Vui lòng chọn ảnh trước khi áp dụng`)
  }
  const uploadTask = firebase.storage().ref(`/pictures/${imageAsFile.name}`).put(imageAsFile)
  //initiates the firebase side uploading
  uploadTask.on('state_changed',
      (snapShot) => {
          //takes a snap shot of the process as it is happening
          console.log(snapShot)
      }, (err) => {
          //catches the errors
          console.log(err)
      }, 
       () => {
          firebase.storage().ref('pictures').child(imageAsFile.name).getDownloadURL()
          .then(fireBaseUrl => {
            console.log('fireBaseUrl',fireBaseUrl)
          })
      })
}

export const addSong = async (data) =>{
  await firebase.firestore().collection('songs').add(data);
  const message = `created ${data.name} !!!! `
  return message 
}

export const deleteSong = async (data) =>{
  // alert(data.id)
  await firebase.firestore().collection('songs').doc(data.id).delete();
  const message = `deleted ${data.id} !!!!`
  return message 
}

