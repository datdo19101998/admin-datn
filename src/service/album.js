import firebase from "./../firebase";

import {sortBy} from 'lodash';

const ref = firebase.firestore().collection('albums');

export const getAlbums = async () => {
  const querySnapshot = await ref.get();
  const albums = [];
  querySnapshot.forEach((doc) => {
    albums.push({
      id: doc.id,
      ...doc.data(),
    });
  });

  const listAlbums = sortBy(albums, 'name');

  return listAlbums;
};

export const getAlbumDetails = async (data) => {

  const {category} = data;
  const ref2 = firebase.firestore()
    .collection('songs')
    .where('category', 'array-contains', category);
  const querySnapshot = await ref2.get();
  const album = [];
  querySnapshot.forEach((doc) => {
    album.push({
      id: doc.id,
      title: doc.data().name,
      artist: doc.data().singer,
      artwork: doc.data().url.cover,
      url: doc.data().url.play,
      likes: doc.data().likes,
      plays: doc.data().plays,
    });
  });

  const listAlbum = sortBy(album, 'name');

  return listAlbum;
};

export const editAlbum = (data) => {
  const {id, description, name,url,likes,category} = data;
  firebase.firestore().collection('albums').doc(id).update({
    description,
    name,
    url,
    likes,
    category
  }).then(() => {
    alert('Album is updated!');
  });
  const message = `Updated albums ${name}`
  return message;
}

export const addAlbum = (data) => {
  firebase.firestore().collection('albums').add(data).then(() => {
    alert('Album is added!');
  });
  const message = `added albums ${data.name}`
  return message;
}

export const deleteAlbum = async (data) =>{
  await firebase.firestore().collection('albums').doc(data.id).delete().then(() => {
    alert(`Delete album ${data.name}`);
  });;
  const message = `deleted ${data.id} !!!!`
  return message 
}
