import firebase from "./../firebase";

import {sortBy} from 'lodash';

const ref3 = firebase.firestore().collection('singers');

export const getSingers = async () => {
  const querySnapshot = await ref3.get();
  const singers = [];
  querySnapshot.forEach((doc) => {
    singers.push({
      id: doc.id,
      hearts: doc.data().hearts,
      likes: doc.data().likes,
      name: doc.data().name,
      url: doc.data().url,
      category: doc.data().category,
      description: doc.data().description,
    });
  });

  const listSingers = sortBy(singers, 'name')
  return listSingers;
};

export const getSingerDetail = async (category) => {  
  const ref = firebase.firestore().collection('songs').where('category', 'array-contains', category);
  const querySnapshot = await ref.get();
  const songs = [];
  querySnapshot.forEach((doc) => {
    songs.push({
      id: doc.id,
      title: doc.data().name,
      artist: doc.data().singer,
      artwork: doc.data().url.cover,
      url: doc.data().url.play,
      likes: doc.data().likes,
      plays: doc.data().plays,
    });
  });

  const listSongs = sortBy(songs, 'name');
  return listSongs;
};

export const updateSinger = (data) =>{
  const {id, hearts, description, name,url,likes,category} = data;
  firebase.firestore().collection('singers').doc(id).update({
    hearts,
    description,
    name,
    url,
    likes,
    category
  }).then(() => {
    alert('Singer is updated!');
  });
  const message = `updated singer ${name}`
  return message;
}

export const addSinger = async (data) =>{
  await firebase.firestore().collection('singers').add(data).then(() => {
    alert(`added singer ${data.name}`);
  })
}


export const deleteSinger = async (data) =>{
  await firebase.firestore().collection('singers').doc(data.id).delete().then(() => {
    alert(`deleted singer ${data.name}`)
  })
  const message = `deleted ${data.name} !!!!`
  return message 
}
