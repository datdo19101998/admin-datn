import firebase from "./../firebase";

import {sortBy,isEmpty} from 'lodash';

export const postUser = async () => {
  try {
    const userId = firebase.auth().currentUser.uid;
    const getUser = firebase.auth().currentUser;
    const providerData = firebase.auth().currentUser.providerData[0];
    await firebase.firestore()
      .collection('users')
      .doc(userId)
      .collection('infomation')
      .doc(userId)
      .set({
        displayName: providerData.displayName,
        email: providerData.email,
        isAnonymous: getUser.isAnonymous,
        metadata: {
          creationTime: new Date(getUser.metadata.creationTime).toString(),
          lastSignInTime: new Date(getUser.metadata.lastSignInTime).toString(),
        },
        phoneNumber: providerData.phoneNumber,
        photoURL: providerData.photoURL,
        providerData: [
          {
            providerId: providerData.providerId,
            uid: providerData.uid,
          },
        ],
        providerId: providerData.providerId,
        uid: getUser.uid,
      });
    return 'created an user';
  } catch (e) {
    return e.message;
  }
};

export const getCurrentUser = async () => {
  var data = null;
  await firebase.auth().onAuthStateChanged((userChange) => {
    data = userChange;
  });
  return data;
};

export const getRecent = async (id) => {
  const ref3 = firebase.firestore()
    .collection('users')
    .doc(`${id}`)
    .collection('recents');
  const querySnapshot = await ref3.get();
  const songs = [];
  querySnapshot.forEach((doc) => {
    songs.push(doc.data());
  });
  const listRecent = sortBy(songs, 'name');
  return listRecent;
};

export const getListUser = async () => {
  
}

export const getListen = async (date) => {
  if(isEmpty(date)) {
    let ref3 = firebase.firestore().collection('plays');
    let querySnapshot = await ref3.get();

    let songs = [];
    querySnapshot.forEach((doc) => {
      songs.push(doc.data());
    });
    const listSongs = sortBy(songs, 'price', 'desc').reverse();
    return listSongs;
  }else{
    let ref3 = firebase.firestore().collection('plays').where('todayDate', '==',date);
    let querySnapshot = await ref3.get();

    let songs = [];
    querySnapshot.forEach((doc) => {
      songs.push(doc.data());
    });
    const listSongs = sortBy(songs, 'price', 'desc').reverse();
    return listSongs;
  }
};

