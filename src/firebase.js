import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyC2oHMko7AYlaMIDAzgOJNQo-TZ8U4xMsM",
    authDomain: "rnlogin-b2b29.firebaseapp.com",
    databaseURL: "https://rnlogin-b2b29.firebaseio.com",
    projectId: "rnlogin-b2b29",
    storageBucket: "rnlogin-b2b29.appspot.com",
    messagingSenderId: "44995992215",
    appId: "1:44995992215:web:0dfa7c39534ffc94b167d2",
    measurementId: "G-B01TCQRWXD"
};
const firebaseApp = firebase.initializeApp(config);

export default firebaseApp;
